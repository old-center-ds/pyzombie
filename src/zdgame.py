from enum import IntEnum
import random

class DiceType(IntEnum):
    EASY= 0
    MEDIUM= 1
    HARD= 2
    UNDEF= 3

class DiceFace(IntEnum):
    BRAIN= 0
    GUN= 1
    RUN= 2

DiceType_str= ["EASY", "MEDI", "HARD","UNDEF"]
DiceFace_str= ["Brain", "Gun", "Run"]
DiceFace_weight= [
    [3, 1, 2 ], #EASY
    [2, 2, 2 ], #MEDIUM
    [1, 3, 2 ], #HARD
    [0, 0, 1 ], #UNDEF
]

class Dice():
    x= 0

    # Instance initialization
    #------------------------
    def __init__(self):
        self._type= DiceType.UNDEF
        self._face= DiceFace.RUN

    def __str__(self):
        if self._type == DiceType.UNDEF :
            return "UNDEFINED"
        return DiceType_str[self._type] +"("+ DiceFace_str[self._face] +")"

    def csv(self, c):
        return DiceType_str[self._type] +  c + DiceFace_str[self._face]

    # Accessors
    #----------
    def type(self) :
        return self._type

    def face(self) :
        return self._face

    # Builder
    #--------
    def setType(self, type):
        self._type= type
        self._face= DiceFace.RUN

    # Engine
    #-------
    def random_face(self):
        weight= DiceFace_weight[self._type]
        rand= random.randrange( sum( weight ) )
        #print("random "+ str(rand) + " in [0, "+ str(sum(weight)) +"]")
        for face in DiceFace :
            if rand < weight[face] :
                return face
            rand-= weight[face]
        return DiceFace.RUN

    def roll(self):
        self._face= self.random_face()

class Game():

    # Instance initialization
    #------------------------
    def __init__(self, nbDice, stockEASY, stockMEDIUM, stockHARD):
        self._hand= [0]*nbDice
        for i in range(0, nbDice) :
            self._hand[i]= Dice()
        self._brain= 0
        self._shot= 0
        self._stock= [stockEASY, stockMEDIUM, stockHARD]

    # Accessor
    #---------
    def brain(self) :
        return self._brain

    def shot(self) :
        return self._shot

    def stock(self, type):
        return self._stock[type]

    def handSize(self):
        return len(self._hand)

    def handDieType(self, idice):
        if self._hand[idice]._face == DiceFace.RUN :
            return self._hand[idice]._type
        return DiceType.UNDEF

    def remaining_dice(self) :
        count= sum(self._stock)
        for die in self._hand :
            if die.face() == DiceFace.RUN :
                count+= 1
        return count

    def remaining_dice(self) :
        count= sum(self._stock)
        for die in self._hand :
            if die.face() == DiceFace.RUN :
                count+= 1
        return count

    # String
    #-------
    def __str__(self):
        stockSum= 0
        for s in self._stock :
            stockSum= stockSum + s

        myStr= "Stock("+ str(stockSum) +"/"+ str(self.remaining_dice()) +"): "
        myStr+= "EASY("+ str(self._stock[DiceType.EASY]) +") "
        myStr+= "MEDIUM("+ str(self._stock[DiceType.MEDIUM]) +") "
        myStr+= "HARD("+ str(self._stock[DiceType.HARD]) +") "
        myStr+= "\t| Dice: "+ str(self._hand[0])
        for i in range(1, len(self._hand) ) :
            myStr+= " - "+ str(self._hand[i])

        return myStr +"\t| Brain: "+ str(self._brain) +" | Shoot: "+ str(self._shot)

    def csv(self, c):
        myStr= str(sum(self._stock))
        myStr+= c + str(self._stock[DiceType.EASY])
        myStr+= c + str(self._stock[DiceType.MEDIUM])
        myStr+= c + str(self._stock[DiceType.HARD])
        myStr+= c + str(self._brain)
        myStr+= c + str(self._shot)
        for die in self._hand :
            myStr+= c + die.csv(c)
        return myStr

    # Engine
    #-------
    def randomType(self):
        rand= random.randrange( sum(self._stock) )
        #print("random "+ str(rand) + " in [0, "+ str(sum(self._stock)) +"]")
        for type in DiceType :
            if rand < self._stock[type] :
                return type
            rand-= self._stock[type]
        return DiceType.EASY

    def pullRandomDie(self):
        type= self.randomType()
        self._stock[type]-= 1
        return type

    def step(self) :
        for aDie in self._hand :
            # new dice ?
            if aDie.type() == DiceType.UNDEF or not( aDie.face() == DiceFace.RUN ) :
                type= self.pullRandomDie()
                aDie.setType(type)

            # roll and result:
            aDie.roll()
            if aDie.face() == DiceFace.BRAIN :
                self._brain+= 1
            if aDie.face() == DiceFace.GUN :
                self._shot+= 1
