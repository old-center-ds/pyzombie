from abc import ABC, abstractmethod
import random

class Player(ABC) :
    name= ""

    def __init__(self) :
        self._iPlayer= -1

    def setPlayerId(self, id):
        self._iPlayer= id

    # abstract action method
    def act(self, game, scores):
        pass

class Optimist(Player) :

    def act(self, game, scores) :
        return "play"

class Human(Player) :

    def act(self, game, scores) :
        x= "void"
        while not ( x in ("p", "play", "s", "score", "e", "exit") ) :
            print('Action (play, score or exit):')
            x = input()
        return x

class Simple(Player):

    def __init__(self):
        self._told= 0.7

    def act(self, game, scores) :
        if game.brain() < 1 or random.random() < self._told :
            return "play"
        return "score"
