import src.zdgame as zd

class Logger() :

    def __init__(self, fight):
        self._trace= ""
        self._battle= fight

    def reccord(self, iPlayer, score, game, action ):
        txt= str(iPlayer+1)
        for sc in score :
            txt+=", "+str(sc)
        txt+= ", "+ game.csv(", ") +', '+ action
        self._trace+= "\n  - ["+ txt +"]"

    def header(self):
        nb_player= self._battle.nb_player()
        nb_dice= self._battle.nb_dice()
        myStr= "observation:"
        myStr+= "\n  player: " + str( range(1, nb_player+1) )
        for iPlayer in range(0, nb_player ) :
            myStr+= "\n  p"+ str(iPlayer+1) +"_score: " + str( range(0, 25) )
        myStr+= "\n  stock_easy: " + str( range(0, 6) )
        myStr+= "\n  stock_medi: " + str( range(0, 4) )
        myStr+= "\n  stock_hard: " + str( range(0, 3) )
        myStr+= "\n  brain: " + str( range(0, 13) )
        myStr+= "\n  shot: " + str( range(0, 5) )
        for iDie in range(1, nb_dice+1 ) :
            myStr+= "\n  d"+ str(iDie) +"_type: " + self.array_str( zd.DiceType_str )
            myStr+= "\n  d"+ str(iDie) +"_face: " + self.array_str( zd.DiceFace_str )
        myStr+= "\naction:"
        myStr+= "\n  label: [play, score]"
        return myStr

    def __str__(self) :
        return self.header() + "\ntrace: " + self._trace

    def array_str(self, array):
        s= "["+ str(array[0])
        for i in range(1, len(array)) :
            s+= ", "+ str(array[i])
        return s+"]"
