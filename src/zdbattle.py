from enum import Enum
import src.zdgame as zd
from src.zdlogger import Logger

class Mode(Enum):
    SILENT= 0
    VERBOSE= 1

class Battle():

    def __init__(self, nb_dice, players):
        self._player= players
        for i in range(len(self._player)) :
            self._player[i].setPlayerId(i) 
        self._nbDice= nb_dice
        self._score= [0]*len(self._player)
        self._round= 0
        self._mode= Mode.VERBOSE
        self._log= Logger(self)

    def reinit(self):
        self._score= [0]*len(self._player)
        self._round= 0
        self._log= Logger(self)

    # Getter:
    def score(self, iPlayer):
        return self._score[iPlayer]

    def player_name(self, iPlayer):
        return self._player[iPlayer].__class__.__name__

    def mode(self):
        return self._mode

    def nb_player(self):
        return len(self._player)

    def nb_dice(self):
        return self._nbDice

    # mode:
    def setSilentMode(self):
        self._mode= Mode.SILENT

    def setVerboseMode(self):
        self._mode= Mode.VERBOSE

    # String & print:
    def score_str(self):
        rstr= "Round "+ str(self._round) +" Score:"
        for i in range(0, len(self._player) ):
            rstr+= " | "+ self.player_name(i) +": "+ str( self._score[i] )
        return rstr

    def verbose(self, txt):
        if self._mode == Mode.VERBOSE :
            print(txt)

    def log(self):
        return str(self._log)

    # Methode:
    def run(self):
        it= 0
        end= False
        self.reinit()

        while not end :

            self.verbose( "\n\n------------------------------------------------------------------------------------" )
            self.verbose( "\t\t"+ self.score_str()  )
            self.verbose( "------------------------------------------------------------------------------------" )

            self._round= self._round+1
            for iPlayer in range(0, len(self._player)) :
                if self.play_turn(iPlayer) == "exit" :
                    return "exit"
                # Test battle end:
                if self._score[iPlayer] > 12 :
                    end= True

        # Conclusion:
        self.verbose( "\n\n------------------------------------------------------------------------------------" )
        self.verbose( "\t\t"+ self.score_str()  )
        self.verbose( "------------------------------------------------------------------------------------" )

        return "ok"

    def play_turn(self, iPlayer) :
        game= zd.Game(self._nbDice, 6, 4, 3)
        stop= False
        self.verbose( "\n<<<<   Player "+ str(iPlayer) +": "+ self.player_name(iPlayer) +"   >>>>" )

        while not stop :

            self.verbose( game )

            action= self._player[iPlayer].act(game, self._score)
            self._log.reccord( iPlayer, self._score, game, action )
            if action in ("s", "stop", "score") :
                self._score[iPlayer]+= game.brain()
                self.verbose( "Score -> " + str(game.brain()) +" Brains" );
                stop= True

            elif action in ("e", "exit") :
                return "exit"

            else : # the turn continue:
                self.verbose("Continue...")
                game.step()
                # Test turn forced end:
                if game.shot() > 2 : # Player is dead:
                    self.verbose( game )
                    self.verbose( "\tDEAD !!!!!" );
                    stop= True
                elif game.remaining_dice() < 3 : # stock is empty...
                    self.verbose( game )
                    self.verbose( "\tNO MORE DICE !!!!!" );
                    self._score[iPlayer]+= game.brain()
                    stop= True

        return "ok"
#             {
#                 case "e":
#                 case "exit":
#                     a_stop= true;
#                     break;
#                 case "p":
#                 case "play":
#                     playerChange= !a_game.action_play();
#                     if( game_state == GAME_VERBOSE )
#                         System.out.println( "\tPlay -> " +  a_game.action_str() );
#                     break;
#                 case "s":
#                 case "score":
#                     playerChange= true;
#                     player_score= a_game.action_score();
#                     if( game_state == GAME_VERBOSE )
#                         System.out.println( "\tScore -> " + a_game.action_str() );
#                     break;
#             }

#         	it= it+1
#             player_score= 0
#             score= []
#             for i in range(0, a_score.length ):
#                 score[i]= self.score[i]
#
#             if game_state == GAME_VERBOSE :
#                 print( self.game.str() )
# #            	breath();
#
#             action = "score"
#             if( a_game.ok_to_play() ):
#                 action= a_player[a_iPlayer].act( a_iPlayer, a_game.copy(), score );
#
#             if( game_state == GAME_LOG )
#                 System.out.println( a_game.log(logFile, as_gameId, it, a_iPlayer, a_score[a_iPlayer], action) );
#
#             Boolean playerChange= false;
#             switch (action)
#             {
#                 case "e":
#                 case "exit":
#                     a_stop= true;
#                     break;
#                 case "p":
#                 case "play":
#                     playerChange= !a_game.action_play();
#                     if( game_state == GAME_VERBOSE )
#                         System.out.println( "\tPlay -> " +  a_game.action_str() );
#                     break;
#                 case "s":
#                 case "score":
#                     playerChange= true;
#                     player_score= a_game.action_score();
#                     if( game_state == GAME_VERBOSE )
#                         System.out.println( "\tScore -> " + a_game.action_str() );
#                     break;
#             }
#
#             if( playerChange )
#             {
#                 score_counter[player_score]+= 1;
#                 a_score[a_iPlayer]+= player_score;
#
#                 ++it;
#                 if( game_state == GAME_LOG )
#                     System.out.println( a_game.log(logFile, as_gameId, it, a_iPlayer, a_score[a_iPlayer], "void") );
#
                 # ++a_iPlayer;
                 # if(  a_iPlayer == a_player.length )
                 # {
                 #     a_iPlayer= 0;
                 #     for(int i= 0 ; i < a_player.length ; ++i)
                 #         a_stop= a_stop || (a_score[i] > 12);
                 #
                 #     if( !a_stop )
                 #         ++a_round;
                 # }
#
#                 if( !a_stop && game_state == GAME_VERBOSE ){
#                     System.out.println( "\n\n------------------------------------------------------------------------------------" );
#                     System.out.println( score_str() + " | Round "+ a_round );
#                     System.out.println( "------------------------------------------------------------------------------------" );
#                     System.out.println( "< Player "+ a_iPlayer +" "+ a_player[a_iPlayer].getClass().getName() + " >\n" );
#                 }
#             }
#
#         }
#         a_game.action_score();
#         if( game_state == GAME_VERBOSE )
#         {
#             System.out.println( "\t-> " + a_game.action_str()  + " | Round "+ a_round  );
#             System.out.println( "\n\n------------------------------------------------------------------------------------" );
#             System.out.println( score_str() );
#             System.out.println( "------------------------------------------------------------------------------------" );
#         }
#
#
#         if( game_state == GAME_LOG )
#         try {
# 			logFile.close();
# 		} catch (IOException e) {
# 			// TODO Auto-generated catch block
# 			e.printStackTrace();
# 		}
#     }
