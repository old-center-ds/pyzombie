#!/usr/bin/python3
import sys, os
from enum import IntEnum
from bayesiannet import DiscreteVariable, BayesianVariable, BayesianNetwork

def test(element, test_str):
    s= str(element)
    s= ("OK" if s == test_str else "FAILS"+ " - "+ s)
    print(s+ " |> "+ test_str)

def strList(list, sep= ", "):
    s= "["+ str(list[0])
    for elt in list[1:] :
        s+= sep + str(elt)
    return s + "]"

class Type(IntEnum):
    A= 0
    B= 1
    C= 2

# Build area:

A= DiscreteVariable('A')

B= BayesianVariable('B', list(Type) )
B.dependsTo(A)
B.setContitionsFromList([
    [True, {Type.A: 0.4, Type.C: 0.5}],
    [False, {Type.B: 1.0}]
])

C= BayesianVariable("C", ['police', 'pirate', 'pompier'])
C.dependsTo(A,B)
C.setContitionsFromList([
    [True, Type.A, {'police': 0.3, 'pompier': 0.7}],
    [True, Type.B, {'pirate': 1.0}],
    [True, Type.C, {'police': 0.5, 'pompier': 0.5}],
    [False, Type.A, {'police': 0.1, 'pompier': 0.9}],
    [False, Type.B, {'police': 0.2, 'pompier': 0.8}],
    [False, Type.C, {'police': 0.6, 'pompier': 0.4}]
])

# Test area:
B.setOn( Type.B )
A.equiprobable()
B.updateDistribution()

test(A, "A [True(0.5), False(0.5)]/2")
test(B, "|A|-> B [Type.A(0.2), Type.B(0.5), Type.C(0.25)]/3")

C.updateDistribution()

test(C, "|A, B|-> C [police(0.22750000000000004), pirate(0.25), pompier(0.47250000000000003)]/3")

bn= BayesianNetwork(A,B,C)
bn.update()

print( [ v.name() for v in bn.variables() ] )

test( strList(bn.stateDistribution(), ",\n"),
    """[['True', 'Type.A', 'police'](0.06),
['True', 'Type.A', 'pompier'](0.13999999999999999),
['True', 'Type.C', 'police'](0.125),
['True', 'Type.C', 'pompier'](0.125),
['False', 'Type.B', 'police'](0.1),
['False', 'Type.B', 'pompier'](0.4)]""")

test( C, "|A, B|-> C [police(0.28500000000000003), pompier(0.665)]/3" )

# State transition:
test( strList( bn.transitionTo(B,C), ",\n" ), """[['Type.A', 'police'](0.06),
['Type.A', 'pompier'](0.13999999999999999),
['Type.C', 'police'](0.125),
['Type.C', 'pompier'](0.125),
['Type.B', 'police'](0.1),
['Type.B', 'pompier'](0.4)]""")
