
# DiscreteVariable
class Proba:

    def __init__(self, i, v):
        self.index= i
        self.value= v

    def __str__(self):
        return str(self.index) + "("+ str(self.value) +")"

# DiscreteVariable
class DiscreteVariable :

    # Instance creation:
    #-------------------
    def __init__(self, name, domain= [True,False]) :
        self._name= name
        self._domain= domain
        self._distribution= [ Proba(0, 1.0) ]
        self._parent= []
        self._conditionalDstribution= []

    # Accessors:
    #-----------
    def name(self):
        return self._name

    def domain(self):
        return self._domain

    def valueAt(self, index):
        return self._domain[index]

    def indexDistribution(self):
        return self._distribution

    def completeIndexDistribution(self):
        distrib= [0.0 for val in self.domain() ]
        for proba in self._distribution :
            distrib[ proba.index ]= proba.value
        return distrib

    def distribution(self):
        return { self.valueAt(p.index) : p.value for p in self._distribution }

    def isDependent(self):
        return False

    # Affecting:
    #-----------
    def setOn(self, value):
        self.setAt( self._domain.index(value) )

    def setAt(self, index):
        self._distribution= [ Proba(index, 1.0) ]

    def setDistribution(self, dico):
        self._distribution= [
            Proba( self._domain.index(key), value)
            for key,value in dico.items()
            ]

    def equiprobable(self):
        nbValue= len(self._domain)
        p= 1.0/nbValue
        self._distribution= [ Proba(i, p) for i in range(nbValue) ]

    # Printing:
    #----------
    def __str__(self):
        s= self._name + " ["
        sep= ""
        for proba in self._distribution :
            s+= sep + str(self._domain[ proba.index ])
            s+= "("+ str(proba.value) +")"
            sep= ", "
        return s + "]/"+ str(len(self._domain))
