import copy, os, sys

sys.path.append( os.path.dirname(os.path.abspath(__file__)) )
from bayesian import Proba, DiscreteVariable, BayesianVariable

class State():
    def __init__(self, var, proba):
        self.variable= [ val for val in var ]
        self.probability= proba
        self.value= 0.

    def __str__(self):
        return str([str(v) for v in self.variable]) + "("+ str(self.probability) +")"

class BayesianNetwork():

    # Instance creation:
    #-------------------
    def __init__(self, *varaiables):
        # Variables need to be provided in a coherente order for update
        self._var= list(varaiables)
        self._probadist= [ Proba([0 for v in self._var], 1.0) ]

    # Accessors:
    #-----------
    def variables(self):
        return self._var

    def transitionTo(self, *vars):
        bruteDistrib= self.indexTransitionTo(vars)
        distrib= [ State( [ v.valueAt(i) for v,i in zip(vars, proba.index) ],
                        proba.value )
                for proba in bruteDistrib ]
        return distrib

    def variablesMask(self, vars):
        mask= []
        for v in vars :
            for i in range( len(self._var) ) :
                if self._var[i] == v :
                    mask.append(i)
                    break
        return mask

    def indexTransitionTo(self, vars):
        mask= self.variablesMask(vars)

        # sum of proba...
        reducedDistrib= []
        for pr in self._probadist :
            proba= Proba( [ pr.index[m] for m in mask], pr.value )
            toRecord= True
            for existingproba in reducedDistrib :
                if existingproba.index == proba.index :
                    existingproba.value += proba.value
                    toRecord= False
            if toRecord :
                reducedDistrib.append( proba )

        return reducedDistrib

    # Printing:
    #----------

    # Update:
    #--------
    def update(self):
        # Compute states distribution:
        self._probadist= [ Proba([], 1.0) ]
        for v in self._var :
            self.updateStateDistributionForVariable(v)
        # distribution projection over each varaible:
        for v in self._var :
            distrib= self.transitionTo(v)
            v.setDistribution(
                { st.variable[0]:st.probability for st in distrib }
            )

    def updateStateDistributionForVariable( self, variable ):
        newStDistrib= []
        deep= len( self._probadist[0].index )
        for distribProbability in self._probadist :
            # set variable value
            if variable.isDependent() :
                for i in range(deep) :
                    self._var[i].setAt( distribProbability.index[i] )
                # update variable distribution for this configuration
                variable.updateDistribution()
            # Update new newStDistrib
            for proba in variable.indexDistribution() :
                newStDistrib.append(
                    Proba( distribProbability.index + [proba.index],
                            distribProbability.value * proba.value ) )
        self._probadist= newStDistrib

    # Transition:
    #------------
    def stateIndexDistribution(self):
        return self._probadist

    def stateDistribution(self):
        return [ State( self.stateFromIndex(proba.index), proba.value )
                    for proba in self._probadist ]

    def stateFromIndex(self, index):
        return [ var.valueAt(i) for var, i in zip(self._var, index) ]
