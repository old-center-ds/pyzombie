import copy, os, sys

sys.path.append( os.path.dirname(os.path.abspath(__file__)) )
from variable import Proba, DiscreteVariable

class BayesianVariable(DiscreteVariable) :

    # Instance creation:
    #-------------------
    def __init__(self, name, domain= [True,False]) :
        DiscreteVariable.__init__(self, name, domain)
        self._parent= []
        self._conditionalDstribution= []

    # Accessors:
    #-----------
    def keyDistributionFrom(self, keystate):
        keydistrib= self._condtable
        for dad, valueIndex in zip(self._parent, keystate) :
            keydistrib= keydistrib[valueIndex]
        return keydistrib

    def distributionFrom(self, state):
        dico= {}
        keyd= self.keyDistributionFrom( self.keystateFrom(state) )
        return { self.domain()[ keyproba.index ]: keyproba.value
                    for keyproba in keyd }

    def keystateFrom(self, state):
        return [ dad.domain().index(value) for dad, value in zip(self._parent, state) ]

    def isDependent(self):
        return ( len(self._parent) > 0 )

    # Printing:
    #----------
    def __str__(self):
        s= "|"
        sep= ""
        for dad in self._parent :
            s+= sep + dad.name()
            sep= ", "
        return s + "|-> " + DiscreteVariable.__str__(self)

    # GraphViz:
    #----------
    def dotgraph(self) :
        s="digraph node-"+ self.name() +"\n{"
        s+= self.dotnode()
        return s+"\n}"

    def dotnode(self) :
        s= ""
        for dad in self._parent :
            s+= "\n  " + dad.name() + " -> " + self.name()
        return s

    # Upade distribution from parent:
    #--------------------------------
    def blindKeyStateDistribution(self):
        distrib= []
        # initialize first keystate
        kst= [ 0 for p in self._parent ]
        while kst :
            kstProba= 1.0
            for dad, valIndex in zip(self._parent, kst) :
                kstProba*= dad.completeIndexDistribution()[valIndex];
            if kstProba > 0.0 :
                distrib.append( Proba(kst, kstProba) )
            kst= self.nextKeyState(kst)
        return distrib

    def nextKeyState(self, kst):
        newKst= [i for i in kst ]
        ok= False
        i= len(self._parent)-1
        while not ok and i >= 0 :
            if newKst[i]+1 < len(self._parent[i].domain()) :
                newKst[i]= newKst[i]+1
                ok= True
            else :
                newKst[i]= 0
            i-= 1
        if not ok :
            return []
        return newKst

    def updateDistribution(self):
        if not self._parent :
            return

        return self.updateDistributionFrom( self.blindKeyStateDistribution() )

    def updateDistributionFrom(self, kstDistrib):
        # Initialize the distribution at 0 for each posible value
        distrib= [ 0.0 for val in self.domain() ]
        for kstProba in kstDistrib :
            for proba in self.keyDistributionFrom( kstProba.index ) :
                distrib[ proba.index ]+= kstProba.value * proba.value;

        self._distribution= []
        for i in range(len(distrib)):
            if distrib[i] > 0.0 :
                self._distribution.append( Proba(i, distrib[i]) )


    # Builder:
    #---------
    def dependsTo(self, *parents):
        self._parent= list(parents)

    def setContitionsFromList(self, aListOfConditionalProba):
        nbParents= len(self._parent)
        self.intializeConditionTable()
        for tuple in aListOfConditionalProba:
            self.setCondition( tuple[0:nbParents], tuple[nbParents] )

    def intializeConditionTable(self):
        table= []
        for i in range( len(self._parent)-1, -1, -1 ):
            table= [ copy.deepcopy(table) for val in self._parent[i].domain() ]
        self._condtable= table

    def setCondition(self, state, distribution):
        cursor= self._condtable
        for dad, value in zip(self._parent, state) :
            cursor= cursor[ dad.domain().index(value) ]
        del cursor[:]
        for value in distribution.keys() :
            cursor.append( Proba(self.domain().index(value), distribution[value] ) )
