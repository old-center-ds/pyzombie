#!/usr/bin/python3
import sys, os
from bayesian import DiscreteVariable, BayesianVariable

def test(element, test_str):
    s= str(element)
    s= ("OK" if s == test_str else "FAILS"+ " - "+ s)
    print(s+ " |> "+ test_str)

def strList(list):
    s= "["+ str(list[0])
    for elt in list[1:] :
        s+= ", "+ str(elt)
    return s + "]"

A= DiscreteVariable("A", range(2,7,2))
A.setOn(4)
test(A, "A [4(1.0)]/3")

A.setDistribution( {2:0.4, 4:0.6} )
test(A, "A [2(0.4), 4(0.6)]/3")

A.equiprobable()
test(A, "A [2(0.3333333333333333), 4(0.3333333333333333), 6(0.3333333333333333)]/3")

B= DiscreteVariable("B")
B.setOn(True)
test(B, "B [True(1.0)]/2")
test(B.domain(), "[True, False]")

C= BayesianVariable("C", ["bob", "marie", "pirate"])
C.dependsTo(A,B)

test(C, "|A, B|-> C [bob(1.0)]/3" )

C.setContitionsFromList([
    [2, True, {'bob': 1.0}],
    [2, False, {'marie': 1.0}],
    [4, True, {'pirate': 1.0}],
    [4, False, {'bob': 0.8, 'marie': 0.2}],
    [6, True, {'bob': 0.2, 'marie': 0.4, 'pirate': 0.4}],
    [6, False, {'bob': 0.2, 'marie': 0.3, 'pirate': 0.4}],
])

test( strList(C.blindKeyStateDistribution()),
    "[[0, 0](0.3333333333333333), [1, 0](0.3333333333333333), [2, 0](0.3333333333333333)]")

test( C.distributionFrom([4, False]), "{'bob': 0.8, 'marie': 0.2}" )

C.updateDistribution()
test( strList(C.indexDistribution()),
    "[0(0.39999999999999997), 1(0.13333333333333333), 2(0.4666666666666667)]" )

test( C.distribution(),
    "{'bob': 0.39999999999999997, 'marie': 0.13333333333333333, 'pirate': 0.4666666666666667}" )

# B depends from A
#bn= BayesianNetWork([A,B,C])
#bn.update()

#print(A)
#print(B)
#print(C)
